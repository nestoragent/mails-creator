package com.mails;

import com.mails.lib.Init;
import com.mails.pages.HotmailSignupPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.LocalTime;
import java.util.ArrayList;

/**
 * Created by nestor on 11.04.2017.
 */
public class CreateEmail {

    private volatile ArrayList<String> creativeMarketDesigners = new ArrayList<>();

    @Test()
    public void scrapper() throws InterruptedException {

        WebDriver driver = Init.createWebDriver();
        HotmailSignupPage page = new HotmailSignupPage(driver);
        page.createEmailBox();

//        ArrayList<Thread> threads = new ArrayList<>();
//        LocalProducer producer = new LocalProducer();
//        for (int i = 0; i < 1; i++) {
//            Thread thread = new Thread(producer);
//            threads.add(thread);
//            thread.start();
//        }
//
//        LocalTime timeout = LocalTime.now().plusMinutes(5);
//        for (Thread thread : threads) {
//            while (thread.isAlive() && LocalTime.now().isBefore(timeout))
//                Thread.sleep(5 * 1000);
//        }


    }

    class LocalProducer implements Runnable {

        private int counter;
        private WebDriver driver;
        private ArrayList<String> designersProfile;

        public LocalProducer() {
        }

        @Override
        public void run() {
            driver = Init.createWebDriver();
            HotmailSignupPage page = new HotmailSignupPage(driver);
            try {
                page.createEmailBox();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
