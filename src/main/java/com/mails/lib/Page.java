package com.mails.lib;

import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

/**
 * Created by VelichkoAA on 12.01.2016.
 */
public class Page {

    public static String returnItemFromStashIfExist(final String item) {
        if (item.contains("stash")) {
            String[] items = item.split("_");
            if (null != Init.getStash().get(items[1]))
                return Init.getStash().get(items[1]).toString();
        }
        return item;
    }

    public void press_by_link(WebElement element) throws Exception {
//        System.out.println("Pressed button title = " + getElementTitle(element) + ".");
        element.click();
    }

    public void press_button(WebElement webElement) {
//        System.out.println("Pressed button title = " + getElementTitle(webElement) + ".");
        webElement.click();
    }

    public void fill_field(WebElement element, String text) {
        if (null != text) {
            System.out.println("Fill field, with text = " + text);
            try {
                element.clear();
            } catch (InvalidElementStateException e) {
                System.err.println("Failed to clear web element. Error message = " + e.getMessage());
            }
            for (char c : text.toCharArray()) {
                element.sendKeys(c + "");
            }
//            element.sendKeys(text);
        } else
            System.err.println("For field text == null.");
    }

    public void fillFieldWithSleep(WebElement element, String text) {
        fill_field(element, text);
        try {
            sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void fillFieldWithSleepAnotherClick(WebElement element, String text, WebElement anotherClick) {
        fill_field(element, text);
        press_button(anotherClick);
        try {
            sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void sleep(int i) throws InterruptedException {
        Thread.sleep(i * 1000);
    }
}
