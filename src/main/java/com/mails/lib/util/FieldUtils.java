package com.mails.lib.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by sbt-velichko-aa on 01.03.2016.
 */
public class FieldUtils extends org.apache.commons.lang3.reflect.FieldUtils {

    public static List<Field> getDeclaredFieldsWithInheritanse (Class clazz) {
        List<Field> fields = new ArrayList<>();

        fields.addAll(Arrays.asList(clazz.getDeclaredFields()));

        Class supp = clazz.getSuperclass();

        while (!supp.getName().equals("java.lang.Object")) {
            fields.addAll(Arrays.asList(supp.getDeclaredFields()));
            supp = supp.getSuperclass();
        }

        return fields;
    }

    public static List<Field> getDeclaredFieldsWithInheritanse (Object clazz) {
        return getDeclaredFieldsWithInheritanse(clazz.getClass());
    }
}
