package com.mails.lib;

import com.mails.lib.pageFactory.AutotestError;
import com.mails.lib.util.OsCheck;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Created by VelichkoAA on 12.01.2016.
 */
public class Init {

    private static final Map<String, Object> stash = new HashMap<>();
    private static String os = Props.get("automation.os");
    private static String testInstrument = Props.get("automation.instrument");

//    public static WebDriver getDriver() {
//        if (null == driver) {
//            if (os.equalsIgnoreCase("Android") ||
//                    os.equalsIgnoreCase("IOS"))
//                return getBrowserStackDriver();
//
//            try {
//                createWebDriver();
//            } catch (UnreachableBrowserException e) {
//                System.err.println("Failed to create web driver" + e.getMessage());
//                int i = 2;
//                while (i > 0) {
//                    dispose();
//                    try {
//                        createWebDriver();
//                    } catch (UnreachableBrowserException e1) {
//                        System.err.println("Failed to create web driver. Try count = " + i + ". Error message" + e1.getMessage());
//                    }
//                    i--;
//                }
//            }
//        }
//        return driver;
//    }

    public static WebDriver createWebDriver() {
        WebDriver result;
        DesiredCapabilities capabilities = new DesiredCapabilities();
//        capabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
        File downloadDir = new File("download");
        OsCheck checkOS = new OsCheck();

        if (null == testInstrument) {
            testInstrument = "Firefox";
        }

        switch (BrowserEnum.valueOf(testInstrument)) {
            case Firefox:
//                DesiredCapabilities capabilitiesFirefox = DesiredCapabilities.firefox();
//                FirefoxOptions options = new FirefoxOptions();
//
//                options.addPreference("log", "{level: trace}");
//
//                capabilitiesFirefox.setCapability("marionette", true);
//                capabilitiesFirefox.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options.setBinary(driverBinary));


                File geckoDriver;
                switch (checkOS.getOperationSystemType()) {
                    case Windows:
                        geckoDriver = new File("src\\test\\resources\\webdrivers\\windows\\geckodriver.exe");
                        break;
                    case Linux:
                        geckoDriver = new File("src/test/resources/webdrivers/linux64/geckodriver");
                        break;
                    default:
                        throw new AutotestError("Unsupported system. Os = " + System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH));
                }
//                File pathToBinary = new File("D:\\Programs\\Mozilla Firefox\\firefox.exe");
//                FirefoxBinary ffBinary = new FirefoxBinary(pathToBinary);
                FirefoxProfile firefoxProfile = new FirefoxProfile();
                firefoxProfile.setPreference("enableNativeEvents", true);

                FirefoxOptions options = new FirefoxOptions();
//                options.setBinary(pathToBinary.getAbsolutePath());
                options.toCapabilities();
//                options.setBinary(ffBinary);

                System.setProperty("webdriver.gecko.driver", geckoDriver.getAbsolutePath());
                result = new FirefoxDriver(options);
                break;
            case Chrome:
                HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
                chromePrefs.put("profile.default_content_settings.popups", 0);
                chromePrefs.put("download.default_directory", downloadDir.getAbsolutePath());
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.setExperimentalOption("prefs", chromePrefs);
                DesiredCapabilities cap = DesiredCapabilities.chrome();
                cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                cap.setCapability(ChromeOptions.CAPABILITY, chromeOptions);

                File chromeDriver;
                switch (checkOS.getOperationSystemType()) {
                    case MacOS:
                        chromeDriver = new File("src/test/resources/webdrivers/mac/chromedriver");
                        break;
                    case Windows:
                        chromeDriver = new File("src/test/resources/webdrivers/windows/chromedriver.exe");
                        break;
                    case Linux:
                        if (checkOS.getArchType().equals("64"))
                            chromeDriver = new File("src/test/resources/webdrivers/linux64/chromedriver");
                        else
                            chromeDriver = new File("src/test/resources/webdrivers/linux32/chromedriver");
                        break;
                    default:
                        throw new AutotestError("Unsupported system. Os = " + System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH));
                }
                System.setProperty("webdriver.chrome.driver", chromeDriver.getAbsolutePath());
                capabilities.setBrowserName("chrome");
                result = new ChromeDriver(capabilities);
                break;
            case Tor:
                File torProfileDir;
                File torBinary;
                File geckoDriverTor;
                switch (checkOS.getOperationSystemType()) {
                    case Windows:
                        torProfileDir = new File("src\\test\\resources\\webdrivers\\windows\\Browser\\TorBrowser\\Data\\Browser\\profile.default");
                        torBinary = new File("src\\test\\resources\\webdrivers\\windows\\Browser\\firefox.exe");
                        geckoDriverTor = new File("src\\test\\resources\\webdrivers\\windows\\geckodriver.exe");
                        Assert.assertTrue("Didn't found torProfileDir.", torProfileDir.exists());
                        Assert.assertTrue("Didn't found torBinary.", torBinary.exists());
                        break;
                    default:
                        throw new AutotestError("Unsupported system. Os = " + System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH));
                }
                FirefoxBinary binary = new FirefoxBinary(torBinary);
                FirefoxProfile torProfile = new FirefoxProfile();
                torProfile.setPreference("webdriver.load.strategy", "unstable");
                try {
                    binary.startProfile(torProfile, torProfileDir, "");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                FirefoxProfile profile = new FirefoxProfile();
                profile.setPreference("network.proxy.type", 1);
                profile.setPreference("network.proxy.socks", "127.0.0.1");
                profile.setPreference("network.proxy.socks_port", 9150);
                System.setProperty("webdriver.gecko.driver", geckoDriverTor.getAbsolutePath());
                result = new FirefoxDriver(profile);
                break;
            default:
                capabilities.setBrowserName("firefox");
                result = new FirefoxDriver(capabilities);
                break;
        }
        result.manage().window().maximize();
        return result;
    }

    public static void dispose(WebDriver driver) {
        if (os.equals("Android") || os.equals("IOS")) {
        } else {
            try {
                System.out.println("Checking any alert opened");
                WebDriverWait alertWait = new WebDriverWait(driver, 2);
                Alert alert = driver.switchTo().alert();
                System.out.println("Got and alert: " + alert.getText() + "\n closing it.");
                alert.dismiss();
            } catch (Exception | Error e) {
                System.err.println("No alert opened. Closing webdriver.");
            }
            Set<String> windowsHandlerSet = driver.getWindowHandles();
            try {
                if (windowsHandlerSet.size() > 2) {
                    windowsHandlerSet.stream().forEach((winHandle) -> {
                        driver.switchTo().window(winHandle);
                        ((JavascriptExecutor) driver).executeScript(
                                "var objWin = window.self;" +
                                        "objWin.open('', '_self', '');'" +
                                        "objWin.close();");
                    });
                }

            } catch (Exception e) {
                System.err.println("Failed to kill all of the browser windows. Error message = " + e.getMessage());
            }
        }
        try {
            driver.quit();
        } catch (Exception | Error e) {
            System.err.println("Failed to quit web driver. Error message = " + e.getMessage());
        }
    }

    public static Map<String, Object> getStash() {
        return stash;
    }
}
