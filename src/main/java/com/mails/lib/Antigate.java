package com.mails.lib;

import com.anti_captcha.Api.ImageToText;
import com.anti_captcha.Helper.DebugHelper;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class Antigate {

    public static String getCaptcha(WebDriver driver, WebElement captchaWebElement, int shift) throws InterruptedException {
        String result = "";
        byte[] arrScreen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
        File temp = null;
        try {
            BufferedImage imageScreen = ImageIO.read(new ByteArrayInputStream(arrScreen));
            Dimension capDimension = captchaWebElement.getSize();
            Point capLocation = captchaWebElement.getLocation();
            BufferedImage imgCap = imageScreen.getSubimage(capLocation.x, capLocation.y - shift, capDimension.width, capDimension.height);
            temp = File.createTempFile("tempfile", ".tmp");
            FileOutputStream oss = new FileOutputStream(temp);
            ImageIO.write(imgCap, "png", oss);
            FileUtils.copyFile(temp, new File("temp/test.png"));
            oss.close();
        } catch (IOException e1) {
            System.err.println(e1);
        }

        DebugHelper.setVerboseMode(true);

        ImageToText api = new ImageToText();
        api.setClientKey(Props.get("antiCaptchaKey"));
        api.setFilePath(temp.getAbsolutePath());

        if (!api.createTask()) {
            DebugHelper.out(
                    "API v2 send failed. " + api.getErrorMessage(),
                    DebugHelper.Type.ERROR
            );
        } else if (!api.waitForResult()) {
            DebugHelper.out("Could not solve the captcha.", DebugHelper.Type.ERROR);
        } else {
            DebugHelper.out("Result: " + api.getTaskSolution(), DebugHelper.Type.SUCCESS);
            result = api.getTaskSolution();
        }
        return result;
    }
}
