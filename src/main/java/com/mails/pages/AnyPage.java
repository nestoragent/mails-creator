package com.mails.pages;

import com.mails.lib.Page;
import org.jsoup.Jsoup;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by Nestor on 18.03.2016.
 */
public abstract class AnyPage extends Page {

    // class variable
    final static String LEXICON = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    final static String[] MONTH = {"Month", "January", "February", "March", "April", "May", "June", "July", "August",
            "September", "October", "November", "December"};
    final static String[] DAYS = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16",
            "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28"};
    final static java.util.Random RAND = new java.util.Random();

    public void selectCheckbox(WebElement webElement) throws IllegalAccessException {
        if (!webElement.isSelected())
            press_button(webElement);
    }

    public String randomIdentifier() {
        StringBuilder builder = new StringBuilder();
        while (builder.length() == 0) {
            int length = RAND.nextInt(5) + 3;
            for (int i = 0; i < length; i++) {
                builder.append(LEXICON.charAt(RAND.nextInt(LEXICON.length())));
            }
        }
        return "sen" + builder.toString() + RAND.nextInt(100);
    }

    public String randomName() {
        StringBuilder builder = new StringBuilder();
        while (builder.length() == 0) {
            int length = RAND.nextInt(3) + 3;
            for (int i = 0; i < length; i++) {
                builder.append(LEXICON.charAt(RAND.nextInt(LEXICON.length())));
            }
        }
        return "A" + builder.toString();
    }

    public boolean PageContainsText(String[] findText, WebDriver current) {
        boolean result = false;
        try {
            String html = Jsoup.parse(current.getPageSource()).html().toLowerCase();
            for (String fText : findText) {
                result = html.contains(fText.toLowerCase());
                if (result) break;
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        return result;
    }
    public void scrollTo(WebDriver driver, WebElement element) throws InterruptedException {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        Thread.sleep(1000);
    }
}