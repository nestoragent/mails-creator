package com.mails.pages;

import com.mails.lib.Antigate;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by Nestor on 22.03.2016.
 */
public final class HotmailSignupPage extends AnyPage {

    private WebDriver driver;
    private final static int SHIFT = 400;

    public HotmailSignupPage(WebDriver driver) {
        this.driver = driver;
    }

    public void createEmailBox() throws InterruptedException {
        driver.get("https://signup.live.com/signup.aspx");
        new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.id("FirstName")));

        //get captcha image
        ((JavascriptExecutor)driver).executeScript("scroll(0," + SHIFT + ")");
        sleep(1);
        String captcha = Antigate.getCaptcha(driver, driver.findElement(By.cssSelector("div#hipTemplateContainer img")), SHIFT);
        System.out.println("captcha: " + captcha);
        fill_field(driver.findElement(By.cssSelector("input[id*='wlspispSolutionElement']")), captcha);

        //input params
        press_button(driver.findElement(By.id("liveEasiSwitch")));
        new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.id("MemberName")));
        fillFieldWithSleepAnotherClick(driver.findElement(By.id("FirstName")), randomName(), driver.findElement(By.id("Password")));
//        fillFieldWithSleepAnotherClick(driver.findElement(By.id("LastName")), randomName(), driver.findElement(By.id("Password")));
        fillFieldWithSleepAnotherClick(driver.findElement(By.id("LastName")), "Velichko", driver.findElement(By.id("Password")));

        List<WebElement> success = driver.findElements(By.cssSelector("div.text-success"));
        System.out.println("sizeb: " + success.size());
        System.out.println("dsip: " + success.get(0).isDisplayed());
        while (!success.isEmpty() && !success.get(0).isDisplayed()) {
            fill_field(driver.findElement(By.id("MemberName")), randomIdentifier());
            driver.findElement(By.id("Country")).click();
            sleep(2);
            success = driver.findElements(By.cssSelector("div.text-success"));
            System.out.println("size: " + success.size());
        }

        String password = "TE" + randomIdentifier() + "12";
        fillFieldWithSleepAnotherClick(driver.findElement(By.id("Password")), password, driver.findElement(By.id("FirstName")));
        fillFieldWithSleepAnotherClick(driver.findElement(By.id("RetypePassword")), password, driver.findElement(By.id("FirstName")));

        driver.findElement(By.id("Country")).sendKeys("Russia");
        driver.findElement(By.id("BirthMonth")).sendKeys(MONTH[RAND.nextInt(MONTH.length)]);
        driver.findElement(By.id("BirthDay")).sendKeys(DAYS[RAND.nextInt(DAYS.length)]);
        driver.findElement(By.id("BirthYear")).sendKeys("1990");
        driver.findElement(By.id("Gender")).sendKeys("Male");
        driver.findElement(By.id("PhoneCountry")).sendKeys("Russia");
        fillFieldWithSleepAnotherClick(driver.findElement(By.id("PhoneNumber")), "9135886479",
                driver.findElement(By.cssSelector("input[id*='wlspispSolutionElement']")));


        press_button(driver.findElement(By.id("CredentialsAction")));
    }
}
