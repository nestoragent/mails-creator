# For run

Requirements to machine:
- java 8
- git
- maven

To run test need to execute: `mvn clean install -Dtest=com.dribbble.SendMessages -Dlogin=<login> -Dpassword=<password> -DscrollCount=100`
Available cases for test: 
 - com.dribbble.Scrapper
 - com.dribbble.SendMessages
 
 scrollCount - count of scroll when search designers
 
where -D - this is how maven send value to the tests

Also to configure you can change a properties file:
/src/test/resources/config/